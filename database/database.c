#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <dirent.h>
#define PORT 8080

int server_fd, new_socket, valread;
struct sockaddr_in address;
int opt = 1;
int addrlen = sizeof(address);

char userFile[] = "Database/user/user.txt";
char databaseFolder[] = "Database";
char currentDB[1024] = {0};
int isRoot = 0;
int acc = 0;
 
struct cred{
    char id[1024];
    char password[1024];
} cred;


int auth(char str[])
{
    FILE* file = fopen(userFile, "r");

    char line[1024];
    while (fgets(line, 1024, file))
    {
        if (!strcmp(line, str))
        {
            fclose(file);
            return 1;
        }
    }
    fclose(file);

    return 0;
}

void reconnect(){
    char buffer[1024] = {0}, msg[1024] = {0};
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    valread = read(new_socket, buffer, 1024);

    if(!strcmp(buffer, "0")){
        strcpy(msg, "Successfully connected as root!");
        strcpy(cred.id, "root");
        acc = 1;
        isRoot = 1;
    }

    if(!acc){
        send(new_socket, "DUARR", 10, 0);
        bzero(buffer, 1024);

        valread = read(new_socket, buffer, 1024);

        if(auth(buffer)){
            char strbackup[1024];
            strcpy(strbackup, buffer);
            char* ptr = strbackup;
            char* token;

            int i;
            for (i = 0; token = strtok_r(ptr, ":", &ptr); i++){
                if (i == 0)
                {
                    strcpy(cred.id, token);
                }
                else if (i == 1)
                {
                    strcpy(cred.password, token);
                }
            }
            acc = 1;
            strcpy(msg, "Successfully connected");
			strcat(msg, cred.id);
        }
        else{
            strcpy(msg, "Invalid username or password!");
        }
    }
    send(new_socket, msg, strlen(msg), 0);
}

char* create_user(char str[]){
    char* ptr;
    char msg[1024];
    char uname[1024];
    char upassword[1024];
    bzero(msg, 1024);
    bzero(uname, 1024);
    bzero(upassword, 1024);

    char cmd[1024];
    strcpy(cmd, str);
    char* cmdptr = cmd;
    char* token;
    int i;

    for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++)
    {
        if (i == 2)
        {
            strcpy(uname, token);
        }
        else if (i == 5)
        {
            strncpy(upassword, token, strlen(token) - 1);
        }
        else if ((i == 3 && strcmp(token, "IDENTIFIED")) || (i == 4 && strcmp(token, "BY")) || i > 5)
        {
			strcpy(msg, "Syntax error!");
            return ptr = msg;
        }
    }

    bzero(cmd, 1024);
	strcpy(cmd, uname); strcat(cmd, ":"); strcat(cmd, upassword);
    create_file(userFile, cmd, "a");
    strcpy(msg, "User created successfully!");
    return ptr = msg;
}

void create_file(char fileName[], char str[], char mode[]){
    FILE* file = fopen(fileName, mode);
    fprintf(file, "%s\n", str);
    fclose(file);
}

char* grant_permission(char str[]){
    char* ptr;
    char msg[1024];
    char dbname[1024];
    char userid[1024];
    bzero(msg, 1024);
    bzero(dbname, 1024);
    bzero(userid, 1024);

    int i;
    char parse[1024];
    strcpy(parse, str);
    char* parseptr = parse;
    char* token;

    for (i = 0; token = strtok_r(parseptr, " ", &parseptr); i++)
    {
        if (i == 2)
        {
            strcpy(dbname, token);
        }
        else if (i == 4)
        {
            strncpy(userid, token, strlen(token) - 1);
        }
        else if (i == 3 && strcmp(token, "INTO"))
        {
            strcpy(msg, "Syntax error!");
            return ptr = msg;
        }
    }

    FILE* file = fopen(userFile, "r");
    char line[1024];
    int found = 0;

    while (fgets(line, 1024, file))
    {
        if (!strncmp(line, userid, strlen(userid)))
        {
            found = 1;
            break;
        }
    }
    fclose(file);

    if (!found)
    {
        strcpy(msg, "User not found!");
        return ptr = msg;
    }

    char filename[1024];
    sprintf(filename, "%s/%s/granted_user.txt", databaseFolder, dbname);
    
    file = fopen(filename, "a");
    if (!file)
    {
        strcpy(msg, "Database does not exist!");
        return ptr = msg;
    }
    fprintf(file, "%s\n", userid);
    fclose(file);

    strcpy(msg, "Access granted!");
    return ptr = msg;
}

char* create_table(char str[]){
	char* ptr;
	char msg[1024];

	char tableName[1024];
	
	int i = 0;
	char cmd[1024];
	strcpy(cmd, str);
	char* cmdptr = cmd;
	char* token;

	for (i = 0; token = strtok_r(cmdptr, " ", &cmdptr); i++)
	{
		if (i == 2)
		{
			strcpy(tableName, token);
		}
	}

	strcpy(cmd, strcpy);
	cmdptr = strchr(cmd, '(') + 1;

	strncpy(cmd, cmdptr, strlen(cmdptr) - 2);
	char temp[1024];
	strcpy(temp, cmd);
	char table_path[1024];
	sprintf(table_path, "%s/%s.tsv", currentDB, tableName);

	FILE* file = fopen(table_path, "w");
	char* token2;
	int j;
	
	char columnName[1024], dataType[1024];
	for (i = 0; token = strtok_r(cmdptr, ",", &cmdptr); i++)
	{
		char column[1024];
		char* columnptr;
		strcpy(column, token);
		for (j = 0; token2 = strtok_r(columnptr, " ", &columnptr); j++)
		{
			if (i == 0)
			{
				strcpy(columnName, token2);
			}
			if (i == 1)
			{
				strcpy(dataType, token2);
			}
			fprintf(file, "%s:%s", columnName, dataType);
		}
		fprintf(file, "\t");
	}
	fclose(file);

	strcpy(msg, "Table successfully created!");
	return ptr = msg;
}

int remove_directory(const char *path){
	DIR* d = opendir(path);
	size_t path_len = strlen(path);
	int r = -1;
	if(!d) return 1;
   	else{
		struct dirent *p;

		r = 0;
		while (!r && (p=readdir(d))){
          	int r2 = -1;
			char *buf;
			size_t len;

			if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, ".."))
				continue;

			len = path_len + strlen(p->d_name) + 2; 
			buf = malloc(len);

			if (buf) {
				struct stat statbuf;

				snprintf(buf, len, "%s/%s", path, p->d_name);
				if (!stat(buf, &statbuf)) {
					if (S_ISDIR(statbuf.st_mode))
					r2 = remove_directory(buf);
					else
					r2 = unlink(buf);
				}
				free(buf);
			}
			r = r2;
		}
		closedir(d);
	}

	if(!r) r = rmdir(path);
	return r;
}

char* drop_database(char str[]){
	char* ptr;
	char msg[1024];

	char dbname[1024];
	bzero(dbname, 1024);

	int i;
	char parse[1024];
	strcpy(parse, str);
	char* parseptr = parse;
	char* token;

	for (i = 0; token = strtok_r(parseptr, " ", &parseptr); i++){
		if (i == 2){
			strncpy(dbname, token, strlen(token) - 1);
		}
	}

	char databasePath[1024];
	sprintf(databasePath, "%s/%s", databaseFolder, dbname);
	char* pathdbptr = databasePath;

	if(!opendir(databasePath)){
		strcpy(msg, "Database does not exist!");
		ptr = msg;
		return ptr;
	}

	char permissionfile[1024];
	strcpy(permissionfile, databasePath);
	strcat(permissionfile, "/granted_user.txt");

	printf("%s\n", databasePath);

	FILE* file = fopen(permissionfile, "r");
	char line [1024];
	bzero(line, 1024);
	int permit = 0;

	while (fgets(line, 1024, file)){
		if (!strncmp(line, cred.id, strlen(cred.id))){
			permit = 1;
			break;
		}
	}

	if(permit){
		int rmv = remove_directory(databasePath);
		strcpy(msg, "Database successfully dropped!");
		return ptr = msg;
	}

	strcpy(msg, "User does not have permission!");
	return ptr = msg;
}

char* create_database(char str[]){
	char* ptr;
	char msg[1024];

	char dbname[1024];
	bzero(dbname, 1024);

	int i;
	char parse[1024];
	strcpy(parse, str);
	char* parseptr = parse;
	char* token;

	for (i = 0; token = strtok_r(parseptr, " ", &parseptr); i++){
		if (i == 2){
			strncpy(dbname, token, strlen(token) - 1);
		}
	}

	char databasePath[1024];
	sprintf(databasePath, "%s/%s", databaseFolder, dbname);
	char* pathdbptr = databasePath;

	if (mkdir(pathdbptr, 0777) != 0){
		strcpy(msg, "Cannot create database!");
		return ptr = msg;
	}

	char permissionfile[1024];
	strcpy(permissionfile, databasePath);
	strcat(permissionfile, "/granted_user.txt");

	create_file(permissionfile, cred.id, "a");

	strcpy(msg, "Database successfully created!");
	return ptr = msg;
}

char* use(char str[]){
    char* ptr;
    char msg[1024];
    char dbname[1024];
    char permissionFile[1024];
    bzero(msg, 1024);
    bzero(dbname, 1024);
    bzero(permissionFile, 1024);

    char* dbptr = str + 4;
    strncpy(dbname, dbptr, strlen(dbptr) - 1);
    sprintf(permissionFile, "%s/%s/granted_user.txt", databaseFolder, dbname);

    FILE* file = fopen(permissionFile, "r");
    if (!file){
        strcpy(msg, "Database not found!");
		ptr = msg;
		return ptr;
    }

    char line[1024];
    while (fgets(line, 1024, file)){
        if (!strncmp(line, cred.id, strlen(cred.id))){
            fclose(file);
            strcpy(msg, "Database successfully accessed");
            strcpy(currentDB, dbname);
            return ptr = msg;
        }
    }
    fclose(file);
    strcpy(msg, "Database access denied");
    ptr = msg;
    return ptr;
}
 
int main(){
    pid_t pid, sid;
    pid = fork();

    if(pid < 0){
            exit(EXIT_FAILURE);
    }
    
    if(pid > 0) {
            exit(EXIT_SUCCESS);
    }
    
    umask(0);

    sid = setsid();
    if(sid < 0){
            exit(EXIT_FAILURE);
    }

    if((chdir("/")) < 0){
        exit(EXIT_FAILURE);
    }
    
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

	open("/dev/null", O_RDONLY);
	open("/dev/null", O_RDWR);
	open("/dev/null", O_RDWR);

    char buffer[1024] = {0}, msg[1024] = {};
        
    if((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0){
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
        
    if(setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))){
        perror("setsockopt failed");
        exit(EXIT_FAILURE);
    }
        
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);
    
    if(bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0){
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
        
    if(listen(server_fd, 3) < 0){
        perror("listen failed");
        exit(EXIT_FAILURE);
    }
        
    reconnect();
        
    mkdir("Database", 0777);
    mkdir("Database/user", 0777);
    FILE* file = fopen(userFile, "a");
    if(file) fclose(file);
       
    while(1){
        bzero(buffer, 1024);
        valread = read(new_socket, buffer, 1024);
         
        if(!valread){
            acc = 0;
            isRoot = 0;
            reconnect();
            continue;
        }

        printf("<%s>\n", buffer);

        if(buffer[strlen(buffer) - 1] != ';'){
            strcpy(msg, "Syntax error!");
        }
        else if(!strncmp(buffer, "CREATE USER", 11)){
            if(isRoot){
                strcpy(msg, create_user(buffer));
            }
            else
            {
                strcpy(msg, "User does not have this permission!");
            }
        }
        else if(!strncmp(buffer, "GRANT PERMISSION", 16)){
            if(isRoot){
                strcpy(msg, grant_permission(buffer));
            }
            else{
                strcpy(msg, "User does not have this permission!");
            }
        }
        else if(!strncmp(buffer, "USE", 3)){
            strcpy(msg, use(buffer));
        }
		else if(!strncmp(buffer, "CREATE DATABASE", 15)){
			strcpy(msg, create_database(buffer));
		}
		else if(!strncmp(buffer, "DROP DATABASE", 13)){
			strcpy(msg, drop_database(buffer));
		}
		else if(!strncmp(buffer, "CREATE TABLE", 12)){
			strcpy(msg, create_table(buffer));
		}
        else{
            strcpy(msg, "Query unidentified!");
        }
        
        send(new_socket, msg, strlen(msg), 0);
    }

    return 0;
}



